/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 Alberto Gallegos
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Alberto Gallegos <ramonet@fc.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */
#ifndef LBMPACKET_H
#define LBMPACKET_H

#include <iostream>
#include "ns3/header.h"
#include "ns3/enum.h"
#include "ns3/ipv4-address.h"
#include <map>
#include "ns3/nstime.h"

namespace ns3 {
    namespace lbm {
        
        /**
         * \ingroup lbm
         * \brief MessageType enumeration
         */
        enum MessageType
        {
            BROADCASTTYPE = 1,
            BROADCASTTYPE2 = 2,
            HELLOTYPE = 5,
        };
        
        /**
         * \ingroup lbm
         * \brief LBM types
         */
        class TypeHeader : public Header
        {
        public:
            /**
             * constructor
             * \param t the LBM RREQ type
             */
            //TypeHeader (MessageType t = LBMTYPE_RREQ);
            TypeHeader (MessageType t = BROADCASTTYPE);
            
            /**
             * \brief Get the type ID.
             * \return the object TypeId
             */
            static TypeId GetTypeId ();
            TypeId GetInstanceTypeId () const;
            uint32_t GetSerializedSize () const;
            void Serialize (Buffer::Iterator start) const;
            uint32_t Deserialize (Buffer::Iterator start);
            void Print (std::ostream &os) const;
            
            /**
             * \returns the type
             */
            MessageType Get () const
            {
                return m_type;
            }
            /**
             * Check that type if valid
             * \returns true if the type is valid
             */
            bool IsValid () const
            {
                return m_valid;
            }
            /**
             * \brief Comparison operator
             * \param o header to compare
             * \return true if the headers are equal
             */
            bool operator== (TypeHeader const & o) const;
        private:
            MessageType m_type; ///< type of the message
            bool m_valid; ///< Indicates if the message is valid
        };
        
        /**
         * \brief Stream output operator
         * \param os output stream
         * \return updated stream
         */
        std::ostream & operator<< (std::ostream & os, TypeHeader const & h);
        
        
        
        /**
         * \ingroup lbm
         * \brief Route Reply (RREP) Message Format
         \verbatim
         0                   1                   2                   3
         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         |     Type      |R|A|    Reserved     |Prefix Sz|   Hop Count   |
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         |                     Destination IP address                    |
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         |                  Destination Sequence Number                  |
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         |                    Originator IP address                      |
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         |                           Lifetime                            |
         +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         \endverbatim
         */
        
        class BroadcastHeader : public Header
        {
        public:
            /// c-tor
            BroadcastHeader (uint16_t e_start = 0, Time e_time = MilliSeconds(0), uint16_t source = 0, uint16_t e_dst = 0, uint16_t instruction = 0, uint16_t hopCount = 0);
            
            // Header serialization/deserialization
            static TypeId GetTypeId ();
            TypeId GetInstanceTypeId () const;
            uint32_t GetSerializedSize () const;
            void Serialize (Buffer::Iterator start) const;
            uint32_t Deserialize (Buffer::Iterator start);
            void Print (std::ostream &os) const;
            
            // Fields
            void SetE_Start(uint16_t start)
            {
                e_start = start;
            }
            uint16_t GetE_Start() const
            {
                return e_start;
            }
            
            void SetTime(Time time)
            {
                e_time = time.GetMilliSeconds();
            }
            Time GetTime()
            {
                Time t(MilliSeconds(e_time));
                return t;
            }
            
            void SetSource(uint16_t mysource)
            {
                source = mysource;
            }
            uint16_t GetSource() const
            {
                return source;
            }
            
            void SetE_Dst(uint16_t destination)
            {
                e_dst = destination;
            }
            uint16_t GetE_Dst() const
            {
                return e_dst;
            }
            
            void SetInstruction(uint16_t inst)
            {
                instruction = inst;
            }
            uint16_t GetInstruction() const
            {
                return instruction;
            }
            
            void SetHopCount (uint16_t count)
            {
                m_hopCount = count;
            }
            uint16_t GetHopCount () const
            {
                return m_hopCount;
            }
            
            /*void SetDst (Ipv4Address a)
             {
             m_dst = a;
             }*/
            /*Ipv4Address GetDst () const
             {
             return m_dst;
             }*/
            /*void SetOrigin (Ipv4Address a)
             {
             m_origin = a;
             }
             Ipv4Address GetOrigin () const
             {
             return m_origin;
             }*/
            
            
        private:
            uint16_t e_start;
            uint32_t e_time;
            uint16_t source;
            uint16_t e_dst;
            uint16_t instruction;
            uint16_t m_hopCount;
        };
        
        std::ostream & operator<< (std::ostream & os, BroadcastHeader const  &);
        
        
        
        
        class HelloHeader : public Header
        {
        public:
            HelloHeader(uint16_t nodeId = 0, uint16_t position_X = 0, uint16_t position_Y = 0);
            
            static TypeId GetTypeId ();
            TypeId GetInstanceTypeId () const;
            uint32_t GetSerializedSize () const;
            void Serialize (Buffer::Iterator start) const;
            uint32_t Deserialize (Buffer::Iterator start);
            void Print (std::ostream &os) const;
            
            void SetNodeId(uint16_t nodeId)
            {
                m_nodeId = nodeId;
            }
            
            uint16_t GetNodeId()
            {
                return m_nodeId;
            }
            void SetPosition_X(uint16_t position_X)
            {
                m_position_X = position_X;
            }
            
            uint16_t GetPosition_X()
            {
                return m_position_X;
            }
            void SetPosition_Y(uint16_t position_Y)
            {
                m_position_Y = position_Y;
            }
            
            uint16_t GetPosition_Y()
            {
                return m_position_Y;
            }
            
            
        private:
            uint16_t m_nodeId;
            uint16_t m_position_X;
            uint16_t m_position_Y;
        };
        std::ostream & operator<< (std::ostream & os, HelloHeader  const &);
        
        
        
        
    }  // namespace lbm
}  // namespace ns3

#endif /* LBMPACKET_H */
