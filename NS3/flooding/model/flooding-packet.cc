/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 Alberto Gallegos
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Alberto Gallegos <ramonet@fc.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */
#include "flooding-packet.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"

namespace ns3 {
    namespace flooding {
        
        NS_OBJECT_ENSURE_REGISTERED (TypeHeader);
        
        TypeHeader::TypeHeader (MessageType t)
        : m_type (t),
        m_valid (true)
        {
        }
        
        TypeId
        TypeHeader::GetTypeId ()
        {
            static TypeId tid = TypeId ("ns3::flooding::TypeHeader")
            .SetParent<Header> ()
            .SetGroupName ("Flooding")
            .AddConstructor<TypeHeader> ()
            ;
            return tid;
        }
        
        TypeId
        TypeHeader::GetInstanceTypeId () const
        {
            return GetTypeId ();
        }
        
        uint32_t
        TypeHeader::GetSerializedSize () const
        {
            return 1;
        }
        
        void
        TypeHeader::Serialize (Buffer::Iterator i) const
        {
            //i.WriteU8 ((uint16_t) m_type);
            i.WriteU16 ((uint16_t) m_type);
        }
        
        uint32_t
        TypeHeader::Deserialize (Buffer::Iterator start)
        {
            Buffer::Iterator i = start;
            uint16_t type = i.ReadU8 ();
            //uint16_t type = i.ReadU16 ();
            
            m_valid = true;
            switch (type)
            {
                case BROADCASTTYPE:
                case BROADCASTTYPE2:
                case HELLOTYPE:
                {
                    m_type = (MessageType) type;
                    break;
                }
                default:
                    m_valid = false;
            }
            uint32_t dist = i.GetDistanceFrom (start);
            NS_ASSERT (dist == GetSerializedSize ());
            return dist;
        }
        
        void
        TypeHeader::Print (std::ostream &os) const
        {
            switch (m_type)
            {
                case BROADCASTTYPE:
                {
                    os << "Broadcast-sender";
                    break;
                }
                case BROADCASTTYPE2:
                {
                    os << "Broadcast-node";
                    break;
                }
                case HELLOTYPE:
                {
                    os << "Hello";
                    break;
                }
                default:
                    os << "UNKNOWN_TYPE";
            }
        }
        
        bool
        TypeHeader::operator== (TypeHeader const & o) const
        {
            return (m_type == o.m_type && m_valid == o.m_valid);
        }
        
        std::ostream &
        operator<< (std::ostream & os, TypeHeader const & h)
        {
            h.Print (os);
            return os;
        }
        
        //-----------------------------------------------------------------------------
        // Broadcast
        //-----------------------------------------------------------------------------
        BroadcastHeader::BroadcastHeader (uint16_t e_start, Time time,  uint16_t source, uint16_t e_dst, uint16_t instruction, uint16_t hopCount)
        : m_hopCount (hopCount)
        {
            e_time = uint32_t (time.GetMilliSeconds());
        }
         //m_origin (origin)
        
        TypeId
        BroadcastHeader::GetTypeId ()
        {
            static TypeId tid = TypeId ("ns3::flooding::BroadcastHeader")
            .SetParent<Header> ()
            .SetGroupName("Flooding")
            .AddConstructor<BroadcastHeader> ()
            ;
            return tid;
        }
        
        TypeId
        BroadcastHeader::GetInstanceTypeId () const
        {
            return GetTypeId ();
        }
        
        uint32_t
        BroadcastHeader::GetSerializedSize () const
        {
            //8bits(start)+32bits(time)+8bits(source)+8bits(destination)+8bits(instruction)+8bits(hopCount)
            //return 9;
            return 14;
        }
        
        void
        BroadcastHeader::Serialize (Buffer::Iterator i) const
        {
           /* i.WriteU8 (e_start);
            i.WriteHtonU32(e_time);
            i.WriteU8 (source);
            i.WriteU8 (e_dst);
            i.WriteU8 (instruction);
            i.WriteU8 (m_hopCount);
            */
            i.WriteU16 (e_start);
            i.WriteHtonU32(e_time);
            i.WriteU16 (source);
            i.WriteU16 (e_dst);
            i.WriteU16 (instruction);
            i.WriteU16 (m_hopCount);
        }
        
        uint32_t
        BroadcastHeader::Deserialize (Buffer::Iterator start)
        {
            Buffer::Iterator i = start;
            /*
            e_start = i.ReadU8 ();
            e_time = i.ReadNtohU32();
            source = i.ReadU8 ();
            e_dst = i.ReadU8 ();
            instruction = i.ReadU8 ();
            m_hopCount = i.ReadU8 ();
            */
            
            e_start = i.ReadU16 ();
            e_time = i.ReadNtohU32();
            source = i.ReadU16 ();
            e_dst = i.ReadU16 ();
            instruction = i.ReadU16 ();
            m_hopCount = i.ReadU16 ();
            
            uint32_t dist = i.GetDistanceFrom (start);
            NS_ASSERT (dist == GetSerializedSize ());
            return dist;
        }
        
        void
        BroadcastHeader::Print (std::ostream &os) const
        {
            os <<" Emergency Start " << e_start
            <<" SendTime "<<e_time
            <<" Source "<< source
            <<" Emergency Destination " << e_dst
            <<" Instruction " << instruction
            <<" Hop Count " << m_hopCount;
        }
        
        std::ostream &
        operator<<(std::ostream & os, BroadcastHeader const & h)
        {
            h.Print(os);
            return os;
        }
        
        
        
        //----------------------------------------------------------------
        //HELLO
        //----------------------------------------------------------------
        HelloHeader::HelloHeader(uint16_t nodeId, uint16_t position_X, uint16_t position_Y)
        :m_nodeId(nodeId),
        m_position_X(position_X),
        m_position_Y(position_Y)
        {
        }
        
        TypeId
        HelloHeader::GetTypeId ()
        {
            static TypeId tid = TypeId ("ns3::flooding::HelloHeader")
            .SetParent<Header> ()
            .SetGroupName("Flooding")
            .AddConstructor<HelloHeader> ()
            ;
            return tid;
        }
        
        TypeId
        HelloHeader::GetInstanceTypeId () const
        {
            return GetTypeId ();
        }
        
        uint32_t
        HelloHeader::GetSerializedSize () const
        {
            //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
            //1bytes + 2bytes + 2bytes= 5bytes
            //return 5;
            return 6;
        }
        
        void
        HelloHeader::Serialize (Buffer::Iterator i) const
        {
            //i.WriteU8 (m_nodeId);
            i.WriteU16 (m_nodeId);
            i.WriteU16 (m_position_X);
            i.WriteU16 (m_position_Y);
        }
        
        uint32_t
        HelloHeader::Deserialize (Buffer::Iterator start)
        {
            Buffer::Iterator i = start;
            
            //m_nodeId = i.ReadU8 ();
            m_nodeId = i.ReadU16 ();
            m_position_X = i.ReadU16 ();
            m_position_Y = i.ReadU16 ();
            
            
            uint32_t dist = i.GetDistanceFrom (start);
            NS_ASSERT (dist == GetSerializedSize ());
            return dist;
        }
        
        void
        HelloHeader::Print (std::ostream &os) const
        {
            os <<" nodeId " << m_nodeId
            <<" Position_X "<<m_position_X
            <<" Position_Y "<<m_position_Y;
        }
        
        std::ostream &
        operator<<(std::ostream & os, HelloHeader const & h)
        {
            h.Print(os);
            return os;
        }
        
    }
}
