/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define NS_LOG_APPEND_CONTEXT                                   \
if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#define BUFFER_SIZE 50

#include "inogrid-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>

#include "ns3/mobility-model.h"
#include "ns3/mobility-helper.h"
#include "ns3/vector.h"

#define NUM_NODES 300

int geocenter[NUM_NODES] = {};
int recvCount = 0; //
int thop = 0;
int nhop = 0;

int controlcost;
int totalcost;

namespace ns3 {
    
    NS_LOG_COMPONENT_DEFINE ("InogridRoutingProtocol");
    
    namespace inogrid {
        NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);
        
        /// UDP Port for INOGRID control traffic
        const uint32_t RoutingProtocol::INOGRID_PORT = 654;
        
        
        RoutingProtocol::RoutingProtocol ()
        {
            
        }
        
        TypeId
        RoutingProtocol::GetTypeId (void)
        {
            static TypeId tid = TypeId ("ns3::inogrid::RoutingProtocol")
            .SetParent<Ipv4RoutingProtocol> ()
            .SetGroupName ("Inogrid")
            .AddConstructor<RoutingProtocol> ()
            .AddAttribute ("UniformRv",
                           "Access to the underlying UniformRandomVariable",
                           StringValue ("ns3::UniformRandomVariable"),
                           MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                           MakePointerChecker<UniformRandomVariable> ())
            ;
            return tid;
        }
        
        
        RoutingProtocol::~RoutingProtocol ()
        {
        }
        
        void
        RoutingProtocol::DoDispose ()
        {
            m_ipv4 = 0;
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
                 m_socketAddresses.begin (); iter != m_socketAddresses.end (); iter++)
            {
                iter->first->Close ();
            }
            m_socketAddresses.clear ();
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter2 =
                 m_socketSubnetBroadcastAddresses.begin (); iter2 != m_socketSubnetBroadcastAddresses.end (); iter2++)
            {
                iter2->first->Close ();
            }
            m_socketSubnetBroadcastAddresses.clear ();
            
            Ipv4RoutingProtocol::DoDispose ();
        }
        
        
        void
        RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
        {
            *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
            << "; Time: " << Now ().As (unit)
            << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
            << ", INOGRID Routing table" << std::endl;
            
            //Print routing table here.
            *stream->GetStream () << std::endl;
        }
        
        int64_t
        RoutingProtocol::AssignStreams (int64_t stream)
        {
            NS_LOG_FUNCTION (this << stream);
            m_uniformRandomVariable->SetStream (stream);
            return 1;
        }
        
        
        
        Ptr<Ipv4Route>
        RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                                      Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
        {
            
            std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            Ptr<Ipv4Route> route;
            
            if (!p)
            {
                std::cout << "loopback occured! in routeoutput";
                return route;// LoopbackRoute (header,oif);
                
            }
            
            if (m_socketAddresses.empty ())
            {
                sockerr = Socket::ERROR_NOROUTETOHOST;
                NS_LOG_LOGIC ("No zeal interfaces");
                std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";
                
                Ptr<Ipv4Route> route;
                return route;
            }
            
            
            return route;
        }
        
        
        
        bool
        RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                                     Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                                     MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
        {
            
            std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
            return true;
        }
        
        
        
        void
        RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
        {
            NS_ASSERT (ipv4 != 0);
            NS_ASSERT (m_ipv4 == 0);
            m_ipv4 = ipv4;
        }
        
        void
        RoutingProtocol::NotifyInterfaceUp (uint32_t i)
        {
            NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                             << " interface is up");
            Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
            Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
            if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
            {
                return;
            }
            // Create a socket to listen only on this interface
            Ptr<Socket> socket;
            
            socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvInogrid,this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetLocal (), INOGRID_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketAddresses.insert (std::make_pair (socket,iface));
            
            
            // create also a subnet broadcast socket
            socket = Socket::CreateSocket (GetObject<Node> (),
                                           UdpSocketFactory::GetTypeId ());
            NS_ASSERT (socket != 0);
            socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvInogrid, this));
            socket->BindToNetDevice (l3->GetNetDevice (i));
            socket->Bind (InetSocketAddress (iface.GetBroadcast (), INOGRID_PORT));
            socket->SetAllowBroadcast (true);
            socket->SetIpRecvTtl (true);
            m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));
            
            
            if (m_mainAddress == Ipv4Address ())
            {
                m_mainAddress = iface.GetLocal ();
            }
            
            NS_ASSERT (m_mainAddress != Ipv4Address ());
        }
        
        void
        RoutingProtocol::NotifyInterfaceDown (uint32_t i)
        {
            
        }
        
        void
        RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        void
        RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
        {
            
        }
        
        
        void
        RoutingProtocol::DoInitialize (void)
        {
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            for (int i = 1; i < 7; i++)
            {
                Simulator::Schedule(Seconds(i), &RoutingProtocol::SendHello, this);
                Simulator::Schedule(Seconds(i), &RoutingProtocol::SendGate, this);
                Simulator::Schedule(Seconds(i), &RoutingProtocol::SendIno, this);
                
            }
            
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule(Seconds(i), &RoutingProtocol::SendBroadcast, this);
            }
            
            //結果
            for (int i = 1; i < 7; i++)
            {
                if (id == 0)
                    Simulator::Schedule (Seconds (i), &RoutingProtocol::SimulationResult, this);
            }
            
        }
        
        void
        RoutingProtocol::Send (int des_id)
        {
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel> ();
            int MicroSeconds = Simulator::Now ().GetMicroSeconds ();
            m_start_time[des_id] = MicroSeconds;
        }
        
        // 結果出力
        void
        RoutingProtocol::SimulationResult (void)
        {
            std::cout << "time" << Simulator::Now ().GetSeconds () << "\n";
            printf("recv %d\n", recvCount);
            if (Simulator::Now ().GetSeconds () == SimStartTime + 22)
            {
                int sum_br = 0;
                std::cout << "\n\n----結果---------------------------\n\n";
                for (auto itr = m_start_time.begin (); itr != m_start_time.end (); itr++)
                {
                    std::cout << "des id " << itr->first << "送信開始時刻" << m_start_time[itr->first]
                    << "\n";
                }
                std::cout << "sum_br" << sum_br << "\n";
                double avehop = (double) thop / (double) nhop;
                std::cout << "平均ホップ数：" << avehop << "\n";
                std::cout << "recvCount" << recvCount << "\n";
                double packet_recv_rate = (double) recvCount / (double) m_start_time.size ();
                std::cout << "パケット到達率：" << packet_recv_rate << "\n";
                
            }
        }
        
        //sender's broadcast
        void
        RoutingProtocol::SendBroadcast(void)
        {
            //printf("  SENDBRO\n");
            totalcost += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (INOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                broadcastheader.SetInstruction(0);
                
                
                broadcastheader.SetHopCount(0);
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        void
        RoutingProtocol::RecvInogrid (Ptr<Socket> socket)
        {
            Address sourceAddress;
            Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
            InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
            Ipv4Address sender = inetSourceAddr.GetIpv4 ();
            Ipv4Address receiver;
            
            if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
                receiver = m_socketAddresses[socket].GetLocal ();
            
            
            TypeHeader tHeader (BROADCASTTYPE);
            packet->RemoveHeader (tHeader);
            
            switch(tHeader.Get()){
                case BROADCASTTYPE:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case BROADCASTTYPE2:
                    RecvBroadcast(packet, receiver, sender);
                    break;
                case HELLOTYPE:
                    RecvHello(packet,receiver,sender);
                    break;
                case GATETYPE:
                    RecvGate(packet, receiver, sender);
                    break;
                case INOTYPE:
                    RecvIno(packet, receiver, sender);
                    break;
                case NewGATETYPE:
                    RecvNewGATE(packet, receiver, sender);
                    break;
                case SepGATETYPE:
                    RecvSepGATE(packet, receiver, sender);
                    break;
                default:
                    std::cout << "Unknown Type received in " << receiver << " from " << sender << "\n";
                    break;
                    
            }
            
            
            
        }
        
        //sender's broadcast
        void
        RoutingProtocol::RecvBroadcast(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvBRO\n");
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("arrived destination node, complete.\n");
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t lasthop = broadcastheader.GetHopCount();
                uint16_t lastsource = broadcastheader.GetSource();
                lasthop = lasthop + 1;
                
                thop = thop + lasthop;
                nhop++;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lasthop = %d\n",lasthop);
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
                if(id == 1){
                    recvCount++;
                }
            } else {
                BroadcastHeader broadcastheader;
                packet->RemoveHeader(broadcastheader);
                
                uint16_t hop = broadcastheader.GetHopCount();
                
                if(mypos_x >= 100 && mypos_x <= 900 && mypos_y >= 100 && mypos_y <= 900){
                    if(geocenter[id] == 1){
                        if(onebro[id] == 0){
                            
                            SendBroadcast2(hop+1);
                            
                            printf("recvbroadcast中継 nodeId = %d\n", id);
                        }
                    }
                }
            }
            
        }
        
        //node's broadcast
        void
        RoutingProtocol::SendBroadcast2(uint16_t hop)
        {
            totalcost += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (INOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                BroadcastHeader broadcastheader;
                broadcastheader.SetE_Start(0);
                broadcastheader.SetTime(Now());
                broadcastheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                broadcastheader.SetE_Dst(0);
                
                
                broadcastheader.SetHopCount(hop);
                //Ptr<Packet> packet = Create<Packet> ();
                packet->AddHeader (broadcastheader);
                
                TypeHeader tHeader (BROADCASTTYPE2);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        
        void
        RoutingProtocol::SendHello()
        {
            //printf("sendhello\n");
            totalcost += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
            {
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
                Vector pos = mobility->GetPosition ();
                HelloHeader helloHeader;
                Ptr<Packet> packet = Create<Packet> ();
                helloHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
                helloHeader.SetPosition_X(pos.x);
                helloHeader.SetPosition_Y(pos.y);
                packet->AddHeader(helloHeader);
                
                TypeHeader tHeader (HELLOTYPE);
                packet->AddHeader (tHeader);
                
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 1000)));
                EventId e10 = Simulator::Schedule (jitter, &RoutingProtocol::SendTo, this , socket, packet, destination);
                
            }
        }
        
        void
        RoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
        {
            //printf("SendTo\n");
            socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
        }
        
        void
        RoutingProtocol::RecvHello(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvhello\n");
            HelloHeader helloHeader;
            packet->RemoveHeader(helloHeader);
            uint16_t nodeId = helloHeader.GetNodeId();
            uint16_t pos_x = helloHeader.GetPosition_X();
            uint16_t pos_y = helloHeader.GetPosition_Y();
            
            auto itr = table.find(nodeId);
            if(itr != table.end()){
                table.erase(itr);
                table.insert(std::make_pair(nodeId, pos_x));
            } else{
                table.insert(std::make_pair(nodeId, pos_x));
            }
            
            auto itr2 = table2.find(nodeId);
            if(itr2 != table2.end()){
                table2.erase(itr2);
                table2.insert(std::make_pair(nodeId, pos_y));
            } else {
                table2.insert(std::make_pair(nodeId, pos_y));
            }
        }
        
        
        //GATEpacket send
        void
        RoutingProtocol::SendGate(void)
        {
            totalcost += 1;
            controlcost  += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (INOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                GateHeader gateheader;
                gateheader.SetE_Start(0);
                gateheader.SetTime(Now());
                gateheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                gateheader.SetE_Dst(0);
                
                gateheader.SetInstruction(0);
                
                
                gateheader.SetHopCount(0);
                packet->AddHeader (gateheader);
                
                TypeHeader tHeader (GATETYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        
        //GATEpacket recv
        void
        RoutingProtocol::RecvGate(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvGATE\n");
            
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("GATEarrived destination node, complete.\n");
                GateHeader gateheader;
                packet->RemoveHeader(gateheader);
                
                uint16_t lasthop = gateheader.GetHopCount();
                uint16_t lastsource = gateheader.GetSource();
                lasthop = lasthop + 1;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
                
            } else {
                GateHeader gateheader;
                packet->RemoveHeader(gateheader);
                
                auto itr103 = table.begin();     //x
                auto itr104 = table2.begin();     //y
                
                if(mypos_x >= 100 && mypos_x <= 900 && mypos_y >= 100 && mypos_y <= 900){
                    
                    for(int i = 0; i < 4; i++){   //for grid
                        for(int j = 0; j < 4; j++){
                            
                            // printf("(x,y)=(%d,%d)番目グリッド  左上(%d,%d)--右上(%d,%d)--左下(%d,%d)--右下(%d,%d)  中心座標C(%d,%d)\n",i+1, j+1, 100+i*190,100+j*190, 290+i*190,100+j*190, 100+i*190,290+j*190, 290+i*190, 290+j*190, ((290+i*190)+(100+i*190))/2, ((290+j*190)+(100+j*190))/2);
                            
                            
                            if(mypos_x >= 100+i*190 && mypos_x <= 290+i*190 && mypos_y >= 100+j*190 && mypos_y <= 290+j*190){
                                double mydistance =
                                std::sqrt ((((290+i*190)+(100+i*190))/2 - mypos_x) * (((290+i*190)+(100+i*190))/2 - mypos_x) + (((290+j*190)+(100+j*190))/2 - mypos_y) * (((290+j*190)+(100+j*190))/2 - mypos_y));
                                
                                /*
                                 if(mypos_x >= 10+i*19 && mypos_x <= 29+i*19 && mypos_y >= 10+j*19 && mypos_y <= 29+j*19){
                                 double mydistance =
                                 std::sqrt ((((29+i*19)+(10+i*19))/2 - mypos_x) * (((29+i*19)+(10+i*19))/2 - mypos_x) + (((29+j*19)+(10+j*19))/2 - mypos_y) * (((29+j*19)+(10+j*19))/2 - mypos_y));
                                 */
                                
                                double source_minLenge = 300;
                                uint16_t minId = 0;
                                
                                printf("ID:%d  my_dis = %f\n", id, mydistance);
                                printf("tablesize = %lu\n", table.size());
                                
                                
                                if(table.size() > 0){
                                    while(1){
                                        uint16_t source_pos_x = itr103->second;
                                        uint16_t source_pos_y = itr104->second;
                                        uint16_t source_id = itr103->first;
                                        
                                        //printf("s_id = %d\n", source_id);
                                        //printf("spos_x = %d\n", source_pos_x);
                                        //printf("spos_y = %d\n", source_pos_y);
                                        
                                        
                                        double source_distance =
                                        std::sqrt ((((290+i*190)+(100+i*190))/2 - source_pos_x) * (((290+i*190)+(100+i*190))/2 - source_pos_x) + (((290+j*190)+(100+j*190))/2 - source_pos_y) * (((290+j*190)+(100+j*190))/2 - source_pos_y));
                                        
                                        
                                        /*
                                         double source_distance =
                                         std::sqrt ((((29+i*19)+(10+i*19))/2 - source_pos_x) * (((29+i*19)+(10+i*19))/2 - source_pos_x) + (((29+j*19)+(10+j*19))/2 - source_pos_y) * (((29+j*19)+(10+j*19))/2 - source_pos_y));
                                         */
                                        
                                        //printf("ID:%d  source_dis = %f\n", source_id, source_distance);
                                        
                                        if(source_distance <= source_minLenge){
                                            source_minLenge = source_distance;
                                            minId = source_id;
                                            //printf("Minsource_dis = %f, ID = %d\n", source_minLenge, minId);
                                        }
                                        
                                        
                                        itr103++;
                                        itr104++;
                                        
                                        if(itr103 == table.end())
                                            break;
                                    }
                                } //if tablesize
                                
                                
                                //imp
                                if(mydistance <= source_minLenge){
                                    //printf("Mydis_dis = %f, mindis = %f\n", mydistance, source_minLenge);
                                    source_minLenge = mydistance;
                                    //printf("MyId = %d, minId = %d\n", id, minId);
                                    minId = id;
                                    printf("minID = %d, minDis = %f\n", minId, source_minLenge);
                                    
                                    printf("グリッド座標(%d, %d)\n", i+1, j+1);
                                    
                                    geocenter[id] = 1;
                                    
                                }else{
                                    printf("minID = %d, minDis = %f\n", minId, source_minLenge);
                                    //printf("dis_dis = %f, mindis = %f\n", mydistance, source_minLenge);
                                }
                                
                            }
                        }
                    }
                    
                    printf("SENDGATErecvbroadcast中継 nodeId = %d\n", id);
                    printf("----------------\n");
                    
                }
            }
            
        }
        
        
        
        //INOpacket send
        void
        RoutingProtocol::SendIno(void)
        {
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(geocenter[id] == 1){
                
                totalcost += 1;
                controlcost  += 1;
                
                for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                     != m_socketAddresses.end (); ++j)
                {
                    
                    Ptr<Socket> socket = j->first;
                    Ipv4InterfaceAddress iface = j->second;
                    Ptr<Packet> packet = Create<Packet> ();
                    
                    /*
                     RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                     packet->AddHeader (rrepHeader);
                     
                     TypeHeader tHeader (INOGRIDTYPE_RREP);
                     packet->AddHeader (tHeader);
                     */
                    
                    InoHeader inoheader;
                    inoheader.SetE_Start(0);
                    inoheader.SetTime(Now());
                    inoheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                    inoheader.SetE_Dst(0);
                    
                    inoheader.SetInstruction(0);
                    
                    
                    inoheader.SetHopCount(0);
                    packet->AddHeader (inoheader);
                    
                    TypeHeader tHeader (INOTYPE);
                    packet->AddHeader (tHeader);
                    
                    
                    // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                    Ipv4Address destination;
                    if (iface.GetMask () == Ipv4Mask::GetOnes ())
                    {
                        destination = Ipv4Address ("255.255.255.255");
                    }
                    else
                    {
                        destination = iface.GetBroadcast ();
                    }
                    socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                    //std::cout<<"broadcast sent\n";
                    
                }
                
            }
        }
        
        
        
        //INOpacket recv
        void
        RoutingProtocol::RecvIno(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvINO\n");
            
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("INOarrived destination node, complete.\n");
                InoHeader inoheader;
                packet->RemoveHeader(inoheader);
                
                uint16_t lasthop = inoheader.GetHopCount();
                uint16_t lastsource = inoheader.GetSource();
                lasthop = lasthop + 1;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
                
            } else {
                InoHeader inoheader;
                packet->RemoveHeader(inoheader);
                
                auto itr113 = table.begin();     //x
                auto itr114 = table2.begin();     //y
                auto itr115 = table.begin();     //x
                auto itr116 = table2.begin();     //y
                auto itr117 = table.begin();     //x
                //auto itr118 = table2.begin();     //y
                
                uint16_t check = 0;
                
                if(mypos_x >= 100 && mypos_x <= 860 && mypos_y >= 100 && mypos_y <= 860){
                    
                    
                    for(int i = 0; i < 4; i++){   //for grid
                        for(int j = 0; j < 4; j++){
                            
                            if(geocenter[id] == 1){
                                
                                
                                double source_minLenge = 300;
                                double source_min_x = 0;
                                double source_min_y = 0;
                                
                                double new_source_minLenge = 300;
                                
                                uint16_t minId = 0;
                                uint16_t new_minId = 0;
                                
                                //printf("ID:%d  my_dis = %f\n", id, mydistance);
                                printf("tablesize = %lu\n", table.size());
                                
                                //double source_gate_pos_x = 0;
                                //double source_gate_pos_y = 0;
                                
                                if(table.size() > 0){
                                    while(1){
                                        uint16_t source_pos_x = itr113->second;
                                        uint16_t source_pos_y = itr114->second;
                                        uint16_t source_id = itr113->first;
                                        
                                        //printf("s_id = %d\n", source_id);
                                        //printf("spos_x = %d\n", source_pos_x);
                                        //printf("spos_y = %d\n", source_pos_y);
                                        
                                        if(geocenter[source_id] == 1){
                                            double source_distance =
                                            std::sqrt ((mypos_x - source_pos_x) * (mypos_x - source_pos_x) + (mypos_y - source_pos_y) * (mypos_x - source_pos_y));
                                            
                                            if(source_distance <= source_minLenge){
                                                source_minLenge = source_distance;
                                                source_min_x = source_pos_x;
                                                source_min_y = source_pos_y;
                                                minId = source_id;
                                                //printf("Minsource_dis = %f, ID = %d\n", source_minLenge, minId);
                                            }
                                        }
                                        
                                        itr113++;
                                        itr114++;
                                        
                                        if(itr113 == table.end())
                                            break;
                                    } //while
                                } //if tablesize
                                
                                double newgeocenter_x = (mypos_x + source_min_x)/2;
                                double newgeocenter_y = (mypos_y + source_min_y)/2;
                                
                                
                                if(table.size() > 0){
                                    while(1){
                                        uint16_t new_source_pos_x = itr115->second;
                                        uint16_t new_source_pos_y = itr116->second;
                                        uint16_t new_source_id = itr115->first;
                                        
                                        //printf("s_id = %d", source_id);
                                        //printf("  spos_x = %d", source_pos_x);
                                        //printf("  spos_y = %d\n", source_pos_y);
                                        
                                        
                                        double new_source_distance =
                                        std::sqrt ((newgeocenter_x - new_source_pos_x) * (newgeocenter_x - new_source_pos_x) + (newgeocenter_y - new_source_pos_y) * (newgeocenter_y - new_source_pos_y));
                                        
                                        
                                        if(new_source_distance <= new_source_minLenge){
                                            new_source_minLenge = new_source_distance;
                                            new_minId = new_source_id;
                                            //printf("Minsource_dis = %f, ID = %d\n", source_minLenge, minId);
                                        }
                                        
                                        
                                        itr115++;
                                        itr116++;
                                        
                                        if(itr115 == table.end())
                                            break;
                                    }
                                }
                                
                                SendNewGATE();
                                geocenter[new_minId] = 1;
                                geocenter[id] = 2;
                                geocenter[minId] = 2;
                                
                                if(table.size() > 0){
                                    while(1){
                                        //uint16_t check_source_pos_x = itr117->second;
                                        //uint16_t check_source_pos_y = itr118->second;
                                        uint16_t check_source_id = itr117->first;
                                        
                                        //printf("s_id = %d", source_id);
                                        //printf("  spos_x = %d", source_pos_x);
                                        //printf("  spos_y = %d\n", source_pos_y);
                                        
                                        if(geocenter[check_source_id] == 1){
                                            check++;
                                            //break;
                                        }
                                        
                                        itr117++;
                                        
                                        if(itr117 == table.end())
                                            break;
                                    }
                                }
                                
                                if(check == 0){
                                    geocenter[new_minId] = 0;
                                    geocenter[id] = 1;
                                    geocenter[minId] = 1;
                                }
                                
                            }
                        }
                    }
                    
                    printf("SENDINOrecvbroadcast中継 nodeId = %d\n", id);
                    printf("----------------\n");
                    
                }
            }
            
        }
        
        
        //NewGATEpacket send
        void
        RoutingProtocol::SendNewGATE(void)
        {
            totalcost += 1;
            controlcost  += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (INOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                NewGATEHeader newgateheader;
                newgateheader.SetE_Start(0);
                newgateheader.SetTime(Now());
                newgateheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                newgateheader.SetE_Dst(0);
                
                newgateheader.SetInstruction(0);
                
                
                newgateheader.SetHopCount(0);
                packet->AddHeader (newgateheader);
                
                TypeHeader tHeader (NewGATETYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        
        //NewGATEpacket recv
        void
        RoutingProtocol::RecvNewGATE(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvINO\n");
            
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("NewGATEarrived destination node, complete.\n");
                NewGATEHeader newgateheader;
                packet->RemoveHeader(newgateheader);
                
                uint16_t lasthop = newgateheader.GetHopCount();
                uint16_t lastsource = newgateheader.GetSource();
                lasthop = lasthop + 1;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
            } else {
                NewGATEHeader newgateheader;
                packet->RemoveHeader(newgateheader);
                
                auto itr117 = table.begin();     //x
                //auto itr118 = table2.begin();     //y
                
                uint16_t check = 0;
                
                if(mypos_x >= 100 && mypos_x <= 860 && mypos_y >= 100 && mypos_y <= 860){
                    
                    if(geocenter[id] == 1){
                        
                        if(table.size() > 0){
                            while(1){
                                //uint16_t check_source_pos_x = itr117->second;
                                //uint16_t check_source_pos_y = itr118->second;
                                uint16_t check_source_id = itr117->first;
                                
                                //printf("s_id = %d", source_id);
                                //printf("  spos_x = %d", source_pos_x);
                                //printf("  spos_y = %d\n", source_pos_y);
                                
                                if(geocenter[check_source_id] == 1){
                                    check++;
                                    //break;
                                }
                                
                                itr117++;
                                //itr118++;
                                
                                if(itr117 == table.end())
                                    break;
                            }
                        }
                        
                        if(check == 0){
                            SendSepGATE();
                            //geocenter[new_minId] = 0;
                            //geocenter[id] = 1;
                            //geocenter[minId] = 1;
                        }
                        
                    }
                    
                    printf("SENDNewGATErecvbroadcast中継 nodeId = %d\n", id);
                    printf("----------------\n");
                    
                }
            }
            
        }
        
        
        //SepGATEpacket send
        void
        RoutingProtocol::SendSepGATE(void)
        {
            totalcost += 1;
            controlcost  += 1;
            
            for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
                 != m_socketAddresses.end (); ++j)
            {
                
                Ptr<Socket> socket = j->first;
                Ipv4InterfaceAddress iface = j->second;
                Ptr<Packet> packet = Create<Packet> ();
                
                /*
                 RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
                 packet->AddHeader (rrepHeader);
                 
                 TypeHeader tHeader (INOGRIDTYPE_RREP);
                 packet->AddHeader (tHeader);
                 */
                
                SepGATEHeader sepgateheader;
                sepgateheader.SetE_Start(0);
                sepgateheader.SetTime(Now());
                sepgateheader.SetSource(m_ipv4->GetObject<Node> ()->GetId ());
                sepgateheader.SetE_Dst(0);
                
                sepgateheader.SetInstruction(0);
                
                
                sepgateheader.SetHopCount(0);
                packet->AddHeader (sepgateheader);
                
                TypeHeader tHeader (SepGATETYPE);
                packet->AddHeader (tHeader);
                
                
                // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
                Ipv4Address destination;
                if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                    destination = Ipv4Address ("255.255.255.255");
                }
                else
                {
                    destination = iface.GetBroadcast ();
                }
                socket->SendTo (packet, 0, InetSocketAddress (destination, INOGRID_PORT));
                //std::cout<<"broadcast sent\n";
                
            }
        }
        
        
        
        //NewGATEpacket recv
        void
        RoutingProtocol::RecvSepGATE(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
        {
            //printf("recvINO\n");
            
            uint16_t mypos_x, mypos_y;
            
            Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
            Vector mypos = mobility->GetPosition ();
            
            mypos_x = mypos.x;
            mypos_y = mypos.y;
            
            uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();
            
            if(mypos_x >= 670 && mypos_x <= 860 && mypos_y >= 670 && mypos_y <= 860){
                printf("----------------------------\n");
                printf("Separrived destination node, complete.\n");
                SepGATEHeader sepgateheader;
                packet->RemoveHeader(sepgateheader);
                
                uint16_t lasthop = sepgateheader.GetHopCount();
                uint16_t lastsource = sepgateheader.GetSource();
                lasthop = lasthop + 1;
                
                printf("recvnodeID = %d\n", id);
                
                printf("lastsource = %d\n",lastsource);
                printf("----------------------------\n");
                
            } else {
                SepGATEHeader sepgateheader;
                packet->RemoveHeader(sepgateheader);
                
                if(geocenter[id] == 2){
                    geocenter[id] = 1;
                }
                
            }
            
        }
        
        std::map<int, int> RoutingProtocol::m_start_time;
        
        
    } //namespace inogrid
} //namespace ns3
